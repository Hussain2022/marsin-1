//planet menu
function toggleMenu() {
    const menuBox = document.getElementById('menu-box');    
    if(menuBox.style.display == "grid") { // if is menuBox displayed, hide it
        menuBox.style.display = "none";
    }
    else { // if is menuBox hidden, display it
        menuBox.style.display = "grid";
    }
}

//signup form
const signup = document.getElementById('signup');
const modal = document.getElementById('id01');
const close = document.getElementById('close');

window.onclick = function(event) {
    if (event.target == signup) {
        signup.style.display = "none";
    }
}

//login form
const modal2 = document.getElementById('id02');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}